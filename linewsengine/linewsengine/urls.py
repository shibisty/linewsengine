"""hello URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from frontend import views
from django.views.generic import TemplateView

urlpatterns = [
    path('', views.index),
    path('404/', TemplateView.as_view(template_name="404.html")),
    path('blank-page/', TemplateView.as_view(template_name="blank-page.html")),
    path('blog-cards/', TemplateView.as_view(template_name="blog-cards.html")),
    path('blog-columns/', TemplateView.as_view(template_name="blog-columns.html")),
    path('blog-medium/', TemplateView.as_view(template_name="blog-medium.html")),
    path('blog-post-audio/', TemplateView.as_view(template_name="blog-post-audio.html")),
    path('blog-post-carousel/', TemplateView.as_view(template_name="blog-post-carousel.html")),
    path('blog-post-disqus/', TemplateView.as_view(template_name="blog-post-disqus.html")),
    path('blog-post-facebook/', TemplateView.as_view(template_name="blog-post-facebook.html")),
    path('blog-post-medium/', TemplateView.as_view(template_name="blog-post-medium.html")),
    path('blog-post-video/', TemplateView.as_view(template_name="blog-post-video.html")),
    path('blog-post/', TemplateView.as_view(template_name="blog-post.html")),
    path('blog-sidebar/', TemplateView.as_view(template_name="blog-sidebar.html")),
    path('blog-timeline/', TemplateView.as_view(template_name="blog-timeline.html")),
    path('blog/', TemplateView.as_view(template_name="blog.html")),
    path('components-accordions/', TemplateView.as_view(template_name="components-accordions.html")),
    path('components-alerts/', TemplateView.as_view(template_name="components-alerts.html")),
    path('components-badges/', TemplateView.as_view(template_name="components-badges.html")),
    path('components-buttons/', TemplateView.as_view(template_name="components-buttons.html")),
    path('components-cards/', TemplateView.as_view(template_name="components-cards.html")),
    path('components-carousel/', TemplateView.as_view(template_name="components-carousel.html")),
    path('components-charts/', TemplateView.as_view(template_name="components-charts.html")),
    path('components-columns/', TemplateView.as_view(template_name="components-columns.html")),
    path('components-data-tables/', TemplateView.as_view(template_name="components-data-tables.html")),
    path('components-dropdowns/', TemplateView.as_view(template_name="components-dropdowns.html")),
    path('components-form-controls/', TemplateView.as_view(template_name="components-form-controls.html")),
    path('components-form-layouts/', TemplateView.as_view(template_name="components-form-layouts.html")),
    path('components-form-plugins/', TemplateView.as_view(template_name="components-form-plugins.html")),
    path('components-google-maps/', TemplateView.as_view(template_name="components-google-maps.html")),
    path('components-hero/', TemplateView.as_view(template_name="components-hero.html")),
    path('components-icons/', TemplateView.as_view(template_name="components-icons.html")),
    path('components-lightbox/', TemplateView.as_view(template_name="components-lightbox.html")),
    path('components-media-player/', TemplateView.as_view(template_name="components-media-player.html")),
    path('components-modals/', TemplateView.as_view(template_name="components-modals.html")),
    path('components-notifications/', TemplateView.as_view(template_name="components-notifications.html")),
    path('components-pagination/', TemplateView.as_view(template_name="components-pagination.html")),
    path('components-popovers/', TemplateView.as_view(template_name="components-popovers.html")),
    path('components-progress-bars/', TemplateView.as_view(template_name="components-progress-bars.html")),
    path('components-promo-boxes/', TemplateView.as_view(template_name="components-promo-boxes.html")),
    path('components-social/', TemplateView.as_view(template_name="components-social.html")),
    path('components-tables/', TemplateView.as_view(template_name="components-tables.html")),
    path('components-tabs/', TemplateView.as_view(template_name="components-tabs.html")),
    path('components-text-editor/', TemplateView.as_view(template_name="components-text-editor.html")),
    path('components-typography/', TemplateView.as_view(template_name="components-typography.html")),
    path('components-utilities/', TemplateView.as_view(template_name="components-utilities.html")),
    path('components-video-background/', TemplateView.as_view(template_name="components-video-background.html")),
    path('components-widgets/', TemplateView.as_view(template_name="components-widgets.html")),
    path('contact/', TemplateView.as_view(template_name="contact.html")),
    path('faq/', TemplateView.as_view(template_name="faq.html")),
    path('forum-create/', TemplateView.as_view(template_name="forum-create.html")),
    path('forum-post/', TemplateView.as_view(template_name="forum-post.html")),
    path('forum-topic/', TemplateView.as_view(template_name="forum-topic.html")),
    path('forums/', TemplateView.as_view(template_name="forums.html")),
    path('full-width/', TemplateView.as_view(template_name="full-width.html")),
    path('gallery/', TemplateView.as_view(template_name="gallery.html")),
    path('game-post/', TemplateView.as_view(template_name="game-post.html")),
    path('games/', TemplateView.as_view(template_name="games.html")),
    path('home-blog/', TemplateView.as_view(template_name="home-blog.html")),
    path('home-coming-soon/', TemplateView.as_view(template_name="home-coming-soon.html")),
    path('home-magazine/', TemplateView.as_view(template_name="home-magazine.html")),
    path('home-streamer/', TemplateView.as_view(template_name="home-streamer.html")),
    path('home-videos/', TemplateView.as_view(template_name="home-videos.html")),
    path('login-2/', TemplateView.as_view(template_name="login-2.html")),
    path('login/', TemplateView.as_view(template_name="login.html")),
    path('profile/', TemplateView.as_view(template_name="profile.html")),
    path('register/', TemplateView.as_view(template_name="register.html")),
    path('review-post/', TemplateView.as_view(template_name="review-post.html")),
    path('reviews/', TemplateView.as_view(template_name="reviews.html")),
    path('video-post/', TemplateView.as_view(template_name="video-post.html")),
    path('videos/', TemplateView.as_view(template_name="videos.html")),

]
